﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class GameOverManager : MonoBehaviour
{
    public TextMeshProUGUI score;
    public TextMeshProUGUI record;
    
    void Start()
    {
        score.text = "Score: " + (PlayerPrefs.GetInt("Score"));
        record.text = "Record: " + (PlayerPrefs.GetInt("Record"));
    }

    public void RetryButton()
    {
        SceneManager.LoadScene("Game");
    }
    public void ExitButton()
    {
        Application.Quit();
    }
}
