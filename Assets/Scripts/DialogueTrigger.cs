﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{

	public Dialogue dialogue;

	public void TriggerDialogue()
	{
		//Busca a DialogueManager y llama a la función StartDialogue p
		FindObjectOfType<DialogueManager>().StartDialogue(dialogue);
	}

}