﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorManager : MonoBehaviour
{
    public GameObject meteorPrefab;
    public float timeLauchMeteor;

    private Coroutine meteorRoutine;
    private float currentTime = 0;

    public bool inRoutine = true;

    /*void Awake(){
        StartCoroutine(LanzaMeteoritos());
    }

    // Update is called once per frame
    /*
    void Update()
    {
        currentTime += Time.deltaTime;
        if(currentTime>timeLauchMeteor){
            currentTime = 0;
            Instantiate(meteorPrefab,new Vector3(10,Random.Range(-5,5)),Quaternion.identity,this.transform);
        }
    }
    */

    public void StartRoutine () 
    {
        meteorRoutine = StartCoroutine(LanzaMeteoritos());
        inRoutine = true;
    }

    public void StopRoutine()
    {
        StopCoroutine(meteorRoutine);
        inRoutine = false;
    }

    IEnumerator LanzaMeteoritos(){
        while(true){
            Instantiate(meteorPrefab,new Vector3(10,Random.Range(-5,5)),Quaternion.identity,this.transform);
            yield return new WaitForSeconds(timeLauchMeteor);
        }
    }

}
