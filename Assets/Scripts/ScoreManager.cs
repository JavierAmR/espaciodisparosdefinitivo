﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScoreManager : MonoBehaviour
{
    [SerializeField] RectTransform lives;
    [SerializeField] TextMeshProUGUI score;
    public PlayerBehaviour Player;


    public int health = 100;

    public int record;
    public int scoreInt=0;
    private int currentLives = 3;

    // Start is called before the first frame update
    void Start()
    {
        scoreInt = 0;
        currentLives = 3;
        score.text = scoreInt.ToString("000000");
        //score.text = scoreInt.ToString();
    }

    public void AddScore(int value){
        scoreInt+=value;
        score.text = scoreInt.ToString("000000");
    }

    public void LoseLife()
    {
        if (Player.lives >= 0)
        {
            lives.GetChild(Player.lives).gameObject.SetActive(false);
        }
    }
    public void WinLife()
    {
        if(Player.lives < 3)
        {
            lives.GetChild(Player.lives).gameObject.SetActive(true);
        }
    }
    
}
