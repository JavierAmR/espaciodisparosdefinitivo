﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Dialogue
{
	//Se puede añadir un icono del personaje si fuera necesario
	//public Sprite characterAvatar;

	//String para el nombre del hablante
	public string charactername;

	//Array de strings para las frases a decir
	[TextArea(3, 10)]
	public string[] sentences;

}