﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public GameObject[] formaciones;
    public float timeLaunchFormation;


    private float currentTime = 0;
    private Coroutine enemyRoutine;

    public bool inRoutine = true;

    public void StartRoutine()
    {
        enemyRoutine = StartCoroutine(LanzaFormacion());
        inRoutine = true;
    }

    public void StopRoutine()
    {
        StopCoroutine(enemyRoutine);
        inRoutine = false;
    }

    IEnumerator LanzaFormacion(){
        int formacionActual=0;
        while(true){
            formacionActual = Random.Range(0,formaciones.Length); 

            Instantiate(formaciones[formacionActual],new Vector3(10,Random.Range(-3,3)),Quaternion.identity,this.transform);
            yield return new WaitForSeconds(timeLaunchFormation);
        }
    }

}