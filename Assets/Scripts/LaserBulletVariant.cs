﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserBulletVariant : MonoBehaviour
{
    public Vector2 direction;
    public float speed;
    float time;

    public bool isMultiple = false;

    //SELECTOR DE FORMA
    public bool cordinated = false;
    public bool binaryWave = false;
    public bool sinWave = false;

    public Sprite weaponIcon;

    void Update()
    {
        //INSTRUCCIÓN DE MOVIMIENTO
        
        transform.Translate(direction * speed * Time.deltaTime);

        //-Onda dependiente del tiempo real o no
        if (cordinated == false)
        {
            time += Time.deltaTime * 2;
            if (time > 360)
            {
                time = 0;
            }
        }
        
        //LÓGICA DE FORMA

        //-Onda Binaria
        if (binaryWave == true && cordinated == true) 
        {
            
            if (Mathf.Sin(Time.time) > 0 && Mathf.Cos(Time.time) > 0) 
            {
                direction.x = 1;
                direction.y = 0;

            }
            else if (Mathf.Sin(Time.time) > 0 && Mathf.Cos(Time.time) < 0|| Mathf.Sin(Time.time) < 0 && Mathf.Cos(Time.time) > 0)
            {
                direction.x = 0;
                direction.y = 1;

            }
            else if (Mathf.Sin(Time.time) < 0 && Mathf.Cos(Time.time) < 0)
            {
                direction.x = -1;
                direction.y = 0;

            }
        }
        else if (cordinated == false && binaryWave == true)
        {
             if (Mathf.Sin(time) > 0 && Mathf.Cos(time) > 0) 
            {
                direction.x = 1;
                direction.y = 0;

            }
            else if (Mathf.Sin(time) > 0 && Mathf.Cos(time) < 0|| Mathf.Sin(time) < 0 && Mathf.Cos(time) > 0)
            {
                direction.x = 0;
                direction.y = 1;

            }
            else if (Mathf.Sin(time) < 0 && Mathf.Cos(time) < 0)
            {
                direction.x = -1;
                direction.y = 0;

            }
        }

        //-Onda sinusoidal
        if (sinWave == true && cordinated == true) 
        {
            direction.x = Mathf.Sin(Time.time);
        }
        else if (sinWave == true && cordinated == false)
        {
            direction.x = Mathf.Sin(time);
        } 
        
    }


    void OnTriggerEnter2D(Collider2D other)
    {
        //DETECTOR DE COLISIÓN DE ELIMINACIÓN
        if (other.gameObject.tag == "Finish" && isMultiple == false)
        {
            Destroy(transform.parent.gameObject);
        }
        else if (isMultiple == true && (other.gameObject.tag == "Finish" || other.gameObject.tag == "Enemy" || other.gameObject.tag == "Meteor")) 
        {
            if (transform.parent.childCount > 1) 
            {
                Destroy(gameObject);
            }
            else 
            {
                Destroy(transform.parent.gameObject);
            }
        }
    }
}
