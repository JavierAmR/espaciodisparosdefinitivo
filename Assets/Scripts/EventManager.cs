﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EventManager : MonoBehaviour
{
    public bool characterSelection = true;
    public PlayerBehaviour selectedCharacter;
    public GameObject selectedGameObject;
    public ShipInput Inputs;
    public Camera camera;

    private int scoreGoal = 100;


    public DialogueTrigger startDialogue;

    public bool inBoss = false;
    public GameObject DialogueCanvas;
    public GameObject ScoreCanvas;
    public GameObject WeaponCanvas;
    public ScoreManager scoreManager;

    public GameObject boss;

    private GameObject[] enemies;
    private GameObject[] players;

    MeteorManager meteorManager;
    EnemyManager enemyManager;
    RaycastHit ray;
    Collider2D coll;

    private void Start()
    {
        meteorManager = (GameObject.Find("MeteorManager")).GetComponent<MeteorManager>();
        enemyManager = (GameObject.Find("EnemyManager")).GetComponent<EnemyManager>();
        startDialogue.TriggerDialogue();
    }

    public void GameOver()
    {
        PlayerPrefs.SetInt("Record", scoreManager.record);
        PlayerPrefs.SetInt("Score", scoreManager.scoreInt);
        SceneManager.LoadScene("GameOver");
    }

    private void Update() 
    {
        if (!boss.activeSelf && meteorManager.inRoutine == false && enemyManager.inRoutine == false && inBoss == false)
        {
            meteorManager.StartRoutine();
            enemyManager.StartRoutine();
        }
       
        if (scoreManager.scoreInt > scoreGoal) 
        {
            inBoss = true;
            meteorManager.StopRoutine();
            enemyManager.StopRoutine();
            enemies = GameObject.FindGameObjectsWithTag("Enemy");
            if (enemies.Length <= 0)
            {
                boss.SetActive(true);
                scoreGoal *= 3;
            }

        }

        if (characterSelection == true && Input.GetMouseButtonDown(0) && !DialogueCanvas.activeSelf) 
        {
            Vector2 rayPos = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
            RaycastHit2D hit = Physics2D.Raycast(rayPos, Vector2.zero, 0f);
            selectedCharacter = hit.collider.GetComponent<PlayerBehaviour>();
            selectedGameObject = hit.collider.gameObject;
            Inputs.ship = selectedCharacter;
            if (Inputs.ship != null) 
            {
                
                scoreManager.Player = selectedCharacter;
                ScoreCanvas.SetActive(true);
                //WeaponCanvas.SetActive(true);
                selectedCharacter.enabled = true;
                characterSelection = false;

                players = GameObject.FindGameObjectsWithTag("Player");
                
                for (int i = 0; i < players.Length; i++) 
                {
                    if (players[i] != selectedGameObject) 
                    {
                        Destroy(players[i]);                        
                    }
                }

                meteorManager.StartRoutine();
                enemyManager.StartRoutine();
            }
            
        }
        
    }

    
}
