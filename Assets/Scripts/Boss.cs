﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{
    

    [SerializeField] BoxCollider2D colisionador;
    [SerializeField] ParticleSystem ps;
    [SerializeField] AudioSource audioSource;

    private float timeCounter;
    private float timeToShoot;

    private float timeShooting;
    private float speedx;

    bool onDialogue;


    private int health = 8;
    private bool isShooting;

    private ScoreManager sm;

    public GameObject dialogueCanvas;
    public DialogueTrigger bossDialogue;

    public EventManager eventManager;

    public int puntuacion = 100;

    [SerializeField] GameObject bullet;

    private void Awake()
    {        

        sm = (GameObject.Find("ScoreCanvas")).GetComponent<ScoreManager>();

        bossDialogue.TriggerDialogue();
        onDialogue = true;

        Inicitialization();
    }

    protected virtual void Inicitialization()
    {
        timeCounter = 0.0f;
        timeToShoot = 1.0f;
        timeShooting = 1.0f;
        speedx = 3.0f;
        isShooting = false;
    }

    protected virtual void EnemyBehaviour()
    {
        timeCounter += Time.deltaTime;

        if (timeCounter > timeToShoot)
        {
            if (!isShooting)
            {
                isShooting = true;
                Instantiate(bullet, this.transform.position, Quaternion.Euler(0, 0, 180), null);
            }
            if (timeCounter > (timeToShoot + timeShooting))
            {
                speedx *= -1;
                timeCounter = 0.0f;
                isShooting = false;
            }
        }
        else
        {
            
            transform.Translate(0, -speedx * Time.deltaTime, 0);
        }

    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        if (!dialogueCanvas.activeSelf) 
        {
            EnemyBehaviour();
        }
        
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Bullet")
        {
            health--;
            if (health <= 0)
            {
                StartCoroutine(DestroyShip());
            }
            
        }
        else if (other.tag == "Finish")
        {
            Destroy(this.gameObject);
        }
    }


    IEnumerator DestroyShip()
    {
        //Sumar puntos
        sm.AddScore(puntuacion);

        //Desactivo el grafico

        //Elimino el BoxCollider2D
        colisionador.enabled = false;

        //Lanzo la partícula
        //ps.Play();

        //Lanzo sonido de explosion
        //audioSource.Play();

        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);
        health = 8;
        eventManager.inBoss = false;
        this.gameObject.SetActive(false);
    }
}
