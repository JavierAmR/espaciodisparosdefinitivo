﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
	public GameObject DialogueCanvas;
	public TextMeshProUGUI characternameText;
	public TextMeshProUGUI dialogueText;

	//Variable para almacenar los strings
	string sentence;

	//Cola de strings
	private Queue<string> sentences;

	
	void Start()
	{
		sentences = new Queue<string>();
	}

	public void StartDialogue(Dialogue dialogue)
	{
		//Activa el canvas
		DialogueCanvas.SetActive(true);

		//Se asigna el texto del nombre del personaje de la clase Dialogue al texto del componente
		characternameText.text = dialogue.charactername;

		//Limpia la lista en caso de haber hecho un dialogo anteriormente
		sentences.Clear();

		//Mete en la cola los strings del array de dialogue
		foreach (string sentence in dialogue.sentences)
		{
			sentences.Enqueue(sentence);
		}

		//Se llama a la primera frase
		DisplayNextSentence();
	}

	public void DisplayNextSentence()
	{
		//Comprobación de que no quedan más frases y entonces finalizar la función
		if (sentences.Count == 0)
		{
			EndDialogue();
			return;
		}
		//Almacenamos la siguiente frase en una variable
		sentence = sentences.Dequeue();

		StopAllCoroutines();
		StartCoroutine(TypeSentence(sentence));
	}

	//Corrutina para añadir los caracteres uno a uno
	IEnumerator TypeSentence(string sentence)
	{
		dialogueText.text = "";
		foreach (char letter in sentence.ToCharArray())
		{
			dialogueText.text += letter;
			yield return null;
		}
	}

	
	void EndDialogue()
	{
		//Desactiva el canvas
		DialogueCanvas.SetActive(false);
	}

}